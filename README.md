# homepage-versammlung-3

![](screenshot-2014-05-18.png)

**License:** GCC BY-NC-SA 3.0 (https://creativecommons.org/licenses/by-nc-sa/3.0/)

## Background

Homepage-versammlung was one of my first websites and was online for many years. 

It started as a Linklist on a german homepage builder at around 2007 and moved on to custom HTML, CSS and later custom PHP Code on a custom CMS.

Visitors could use services like a web catalog, banner exchange, forum, ...

AFIAK this should be the last version of the Design. 